<?php

namespace App\Http\Controllers;

use App\ExcellenceCenterService;
use Illuminate\Http\Request;

class ExcellenceCenterServiceController extends Controller
{
    public function lifeFitness(){
        return view('pakistan-strength.excellence-center-service.life-fitness');
    }
    public function show($url)
    {
        $excellence_center_service = ExcellenceCenterService::where('url',"excellence-center-services/$url")->first();
        return view('pakistan-strength.excellence-center-service.excellence-center-service',compact('excellence_center_service'));
    }
    public function delete($id){
        ExcellenceCenterService::find($id);
    }
}

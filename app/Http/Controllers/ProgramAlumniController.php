<?php

namespace App\Http\Controllers;

use App\Batch;
use App\Student;
use Illuminate\Http\Request;

class ProgramAlumniController extends Controller
{
    public function index($url){

        $batch = Batch::where('url',"program-alumni/$url")->first();

        return view('pakistan-strength.program-alumni.alumni',compact('batch'));
    }
    public function show($id)
    {
        $student = Student::find($id);
        return view('pakistan-strength.program-alumni.alumni-detail',compact('student'));
    }
}

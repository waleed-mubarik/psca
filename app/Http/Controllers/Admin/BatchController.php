<?php

namespace App\Http\Controllers\Admin;

use App\Batch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class BatchController extends Controller
{
    protected $url = 'program-alumni/';
    public function create()
    {
        return view('pakistan-strength.admin.program-alumni.batch');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),['batch_name'=>'required']);
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $batch_name = str_slug($request->batch_name,'-');
        $url = $this->url.$batch_name;
        $batch = new Batch();
        $batch->url = $url;
        $batch->batch_name = $request->batch_name;
        $batch->save();
        return back();
    }

    public function show()
    {
        $batches = Batch::all();
        return view('pakistan-strength.admin.program-alumni.all-batches',compact('batches'));
    }
    public function batchStudents($id)
    {
        $batch_id = Batch::find($id);
        $batch_name = $batch_id->batch_name;
        $batch_students = $batch_id->students;
        return view('pakistan-strength.admin.program-alumni.batch-students',compact('batch_students','batch_name'));

    }

    public function destroy($id)
    {
        $batch = Batch::findorFail($id);
        foreach ($batch->students as $student){
            $student->delete();
        };
        $batch->delete();
        return back();
    }
}

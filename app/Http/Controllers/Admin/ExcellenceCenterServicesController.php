<?php

namespace App\Http\Controllers\Admin;

use App\ExcellenceCenterService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExcellenceCenterServicesController extends Controller
{
    protected $category = 'excellence-center-services/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pakistan-strength.admin.excellence-center-service.services');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pakistan-strength.admin.excellence-center-service.create-service');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();
        $rule = [
            'page_name'=>'required',
            'title' => 'required',
            'description'=>'required',
            'instruction'=>'required',
            'goal'=>'required',
            'image'=>'required|mimes:jpeg'
        ];
        $message = ['page_name.required' => 'please enter the service name'];
       $validator = Validator::make($input,$rule,$message);
       if ($validator->fails()){
           return back()->withErrors($validator)->withInput();
       }


       $url = str_slug($request->page_name,'-');

//        dd($request->instruction);
        if($request->hasFile('image')){
            $filename = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->move('uploads/service-image',$filename);
        }
        $service = $request->only('page_name','title','description','instruction','goal');
        $service['url'] = $this->category.$url;
        $service['image'] = $filename;
        ExcellenceCenterService::create($service);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $all_services = ExcellenceCenterService::all();
        return view('pakistan-strength.admin.excellence-center-service.all-services',compact('all_services'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $excellence_center_service = ExcellenceCenterService::find($id);
//        dd($excellence_center_service);
        return view('pakistan-strength.admin.excellence-center-service.edit-service',compact('excellence_center_service'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('image')){
            $filename = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->move('uploads/service-image',$filename);
        }
        $update = ExcellenceCenterService::find($id);
        $update->page_name = $request->get('page_name');
        $update->goal = $request->get('goal');
        $update->description = $request->get('description');
        $update->title = $request->get('title');
        $update->instruction = $request->get('instruction');
        if($request->hasFile('image')) {
            $update->image = $filename;
        }
        $update->save();
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = ExcellenceCenterService::find($id);
        $delete->delete();
        return redirect('admin/all-excellence-center-services');
    }
}

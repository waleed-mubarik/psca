<?php

namespace App\Http\Controllers\Admin;

use App\Mission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MissionController extends Controller
{
    public function ourMission(){
        $mission = Mission::first();
        return view('pakistan-strength.admin.mission.our-mission',compact('mission'));
    }
    public function storeMission(Request $request){
        Mission::updateOrCreate(['id'=>$request->mission_id],['mission' => $request->mission]);
        return back();
    }
}

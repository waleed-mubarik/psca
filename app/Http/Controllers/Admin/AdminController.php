<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(){
        return view('pakistan-strength.admin.login.login-form');
    }
    public function show()
    {
        $faculty_members = User::where('role_id',2)->get();
        return view('pakistan-strength.admin.faculty.all-faculty-member',compact('faculty_members'));
    }
    public function createFacultyMember(){
        return view('pakistan-strength.admin.faculty.add-faculty-member');
    }
    public function storeFacultyMember(Request $request){
//        dd($request->all());
        if($request->hasFile('avatar')){
            $filename = $request->file('avatar')->getClientOriginalName();
            $request->file('avatar')->move('uploads/avatars',$filename);
        }
        $faculty = $request->only('name','email','city','country','position','sport','description');
        $faculty['avatar'] = $filename;
        User::create($faculty);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Batch;
use App\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class StudentController extends Controller
{
    public function create()
    {
        $batches = Batch::all();
        return view('pakistan-strength.admin.student.add-student',compact('batches'));
    }
    public function store(Request $request)
    {
//        dd($request->all());

        $input = $request->all();
        $rule = [
            'name'=>'required',
            'batch_id'=>'required',
            'gender'=>'required',
//            'data'=>'required'
        ];
        $message = [
            'batch_id.required' => 'Please select the batch',
            'avatar.required' => 'Please select the image'];
        $validator = Validator::make($input,$rule,$message);
        if ($validator->fails()){
//            return back()->withErrors($validator)->withInput();
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $student = new Student();
        $image = $request->data['image'];  //base64 encoded
        $image = str_replace(['data:image/png;base64,','data:image/jpeg;base64,','data:image/jpg;base64,'], '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = $request->name.$student->id.'_'.time().'.'.'jpeg';
        File::put(public_path('uploads/students/avatars/'.$imageName), base64_decode($image));


        $student->name = $request->name;
        $student->batch_id = $request->batch_id;
        $student->gender = $request->gender;
        $student->avatar = $imageName;
        $student->save();

        return response()->json(['success'=>'Record is successfully added']);

    }
    public function show()
    {
        $students = Student::paginate();
        return view('pakistan-strength.admin.student.all-students',compact('students'));
    }
    public function edit($id)
    {
        $student = Student::find($id);
        return view('pakistan-strength.admin.student.edit-student',compact('student'));
    }
    public function update(Request $request, $id)
    {
        $student = Student::find($id);
//        dd($request->data);
        $image = $request->data['image'];  //base64 encoded
        $image = str_replace(['data:image/png;base64,','data:image/jpeg;base64,','data:image/jpg;base64,'], '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = $request->name.$student->id.'_'.time().'.'.'jpeg';
        File::put(public_path('uploads/students/avatars/'.$imageName), base64_decode($image));

//        $student = Student::find($id);
        $student->name = $request->name;
        $student->gender = $request->gender;
        $student->avatar = $imageName;
        $student->save();

//        if($request->hasFile('avatar')){
//            $filename = time().$request->file('avatar')->getClientOriginalName();
//            $request->file('avatar')->move('uploads/students/avatars',$filename);
//        }

        return response()->json(['success' => 'Successfully updated profile','img'=>$imageName]);
    }
}

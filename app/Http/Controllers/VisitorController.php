<?php

namespace App\Http\Controllers;

use App\Mission;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    public function index(){
        $mission = Mission::first();
        return view('pakistan-strength.index',compact('mission'));
    }
    public function introductoryProgram(){
        return view('pakistan-strength.introductory-program');
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class FacultyController extends Controller
{
    public function index(){
        $faculty_member = User::where('role_id',2)->get();
        return view('pakistan-strength.faculty.faculty',compact('faculty_member'));
    }
    public function facultyProfile($id){
        $faculty_member = User::find($id);
        return view('pakistan-strength.faculty.faculty-profile',compact('faculty_member'));
    }
}

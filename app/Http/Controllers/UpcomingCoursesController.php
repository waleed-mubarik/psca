<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UpcomingCoursesController extends Controller
{
    public function index(){
        return view('pakistan-strength.courses.upcoming-courses');
    }
    public function courses(){
        return view('pakistan-strength.courses.courses');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
//            'name' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        if(request()->hasFile('avatar')){
           $filename = request()->file('avatar')->getClientOriginalName();
           request()->file('avatar')->move('uploads/avatars',$filename);
        }
        return  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'city' => $data['city'],
            'country' => $data['country'],
            'position' => $data['position'],
            'description' => $data['description'],
            'sport' => $data['sport'],
            'avatar' => $filename,
//            'password' => Hash::make($data['password']),
        ]);


    }
    public function showRegistrationForm()
    {
        $roles = Role::all();
        return view('pakistan-strength.admin.faculty.add-faculty-member',compact('roles'));
    }
}

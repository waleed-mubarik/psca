<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $guarded = [];

    public function setNameAttribute($value)
    {
        return $this->attributes['name'] = ucwords($value);
    }
}

/*--------------------------------------
		CUSTOM FUNCTION WRITE HERE		
--------------------------------------*/
"use strict";
jQuery(document).on('ready', function() {
	/*--------------------------------------
			MOBILE MENU						
	--------------------------------------*/
	function collapseMenu(){
		jQuery('.at-navigation ul li.menu-item-has-children, .at-navigation ul li.menu-item-has-mega-menu').prepend('<span class="at-dropdowarrow"><i class="fa fa-caret-down"></i></span>');
		jQuery('.at-navigation ul li.menu-item-has-children span, .at-navigation ul li.menu-item-has-mega-menu span').on('click', function() {
			jQuery(this).parent('li').toggleClass('at-open');
			jQuery(this).next().next().slideToggle(300);
		});
	}
	collapseMenu();
	/*--------------------------------------
			Home Slider
	--------------------------------------*/
	$('#at-homeslider').owlCarousel({
		items: 1,
		loop: true,
		nav: true,
		dots:false,
		autoplay: false,
		navigation : true,
		navigationText: [
			"<i class='icon-arrow-right'></i>",
			"<i class=' icon-arrow-right'></i>"
		],
	})
	/*--------------------------------------
			COUNTER
	--------------------------------------*/
	try {
		$('.at-counterbox').appear(function () {
			$('.at-statistic-count').countTo();
		});
	} catch (err) {}
	/* -------------------------------------
				PRETTY PHOTO GALLERY
		-------------------------------------- */
	$("a[data-rel]").each(function () {
		$(this).attr("rel", $(this).data("rel"));
	});
	$("a[data-rel^='prettyPhoto']").prettyPhoto({
		animation_speed: 'normal',
		theme: 'dark_square',
		slideshow: 3000,
		autoplay_slideshow: false,
		social_tools: false
	});
});
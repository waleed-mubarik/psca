<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('check',function (){
    return view('pakistan-strength.admin.faculty.all-faculty-member');
});

Route::get('/','VisitorController@index');
Route::get('introductory-program','VisitorController@introductoryProgram')->name('introductory');
Route::group(['namespace'=>'Visitor'],function (){
    Route::get('apply-online','ApplyOnlineController@index')->name('applyOnline');
});



Route::get('faculty','FacultyController@index')->name('faculty');
Route::get('faculty-profile/{id}','FacultyController@facultyProfile')->name('facultyProfile');
Route::get('upcoming-courses','UpcomingCoursesController@index')->name('upcomingCourses');
Route::get('courses','UpcomingCoursesController@courses')->name('courses');

Route::get('excellence-center-services/{url}','ExcellenceCenterServiceController@show');


Route::get('program-alumni/{url}','ProgramAlumniController@index')->name('programAlumni');
Route::get('alumni-detail/{id}','ProgramAlumniController@show')->name('alumniDetail');

Route::get('admin/login','Admin\AdminController@index')->name('admin');
Route::group(['middleware' => 'admin','namespace'=>'Admin','prefix'=>'admin'],function (){
//    Route::get('/','AdminController@index')->name('admin');
    Route::get('/faculty-members','AdminController@show')->name('admin');
    Route::get('/our-mission','MissionController@ourMission')->name('mission');
    Route::put('/store-mission','MissionController@storeMission')->name('store-mission');
    Route::get('/create-faculty-member','AdminController@createFacultyMember')->name('create-faculty');
    Route::post('/store-faculty-member','AdminController@storeFacultyMember')->name('store-faculty');
    Route::get('/excellence-center-service','ExcellenceCenterServicesController@index')->name('services');
    Route::get('/create-excellence-center-service','ExcellenceCenterServicesController@create')->name('create-service');
    Route::post('/store-excellence-center-service','ExcellenceCenterServicesController@store')->name('store-service');
    Route::get('/all-excellence-center-services','ExcellenceCenterServicesController@show')->name('show-all-services');
    Route::get('/edit-excellence-center-service/{id}','ExcellenceCenterServicesController@edit')->name('edit-service');
    Route::put('/update-excellence-center-service/{id}','ExcellenceCenterServicesController@update')->name('update-service');
    Route::delete('delete-service/{id}','ExcellenceCenterServicesController@destroy')->name('delete-service');

    Route::get('add-batch','BatchController@create')->name('add-batch');
    Route::post('store-batch','BatchController@store')->name('store-batch');
    Route::delete('delete-batch/{id}','BatchController@destroy')->name('delete-batch');
    Route::get('all-batch','BatchController@show')->name('all-batch');
    Route::get('batch-students/{id}','BatchController@batchStudents')->name('batch-students');

    Route::get('add-student','StudentController@create')->name('add-student');
    Route::post('store-student','StudentController@store')->name('store-student');
    Route::get('all-student','StudentController@show')->name('all-student');
    Route::get('edit-student/{id}','StudentController@edit')->name('edit-student');
    Route::put('update-student/{id}','StudentController@update')->name('update-student');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

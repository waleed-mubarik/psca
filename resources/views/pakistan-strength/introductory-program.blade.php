@extends('pakistan-strength.layouts.master')
@section('title','Program Info')
@section('headings')
    <h1>Introductory <em>Program</em></h1>--}}
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
            Program Information Start
        *************************************-->
        <div class="at-programinfo at-haslayout">
            <div class="container">
                <div class="at-programinfoarea">
                    <div class="at-aboutuscontentbox">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <div class="at-ourmissioncontent">
                                    <div class="at-sectionheading">
                                        <h2>Our Structure</h2>
                                    </div>
                                    <div class="at-description">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                                        <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <figure class="at-ourmission-img">
                                    <img src="images/aboutus/01.jpg" alt="image description">
                                    <img src="images/aboutus/02.jpg" class="at-overlay" alt="image description">
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="at-aboutuscontentbox">
                        <div class="at-candidateprojectholder">
                            <ul class="at-candidateproject">
                                <li><span>Archery</span></li>
                                <li><span>Athletics</span></li>
                                <li><span>Basketball</span></li>
                                <li><span>Boxing</span></li>
                                <li><span>Field Hockey</span></li>
                                <li><span>Fencing</span></li>
                                <li><span>Judo</span></li>
                                <li><span>Rowing</span></li>
                                <li><span>Rugby</span></li>
                                <li><span>Swimming</span></li>
                                <li><span>Table Tennis</span></li>
                                <li><span>Taekwondo</span></li>
                                <li><span>Tennis</span></li>
                                <li><span>Volleyball</span></li>
                                <li><span>Weightlifting</span></li>
                                <li><span>Wrestling</span></li>
                            </ul>
                        </div>
                        <div class="at-candidateprojectcontent">
                            <div class="at-programinfoheading">
                                <h2>International Candidates Personal Project:</h2>
                            </div>
                            <div class="at-description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
                            </div>
                        </div>
                    </div>
                    <div class="at-aboutuscontentbox">
                        <div class="at-programinfoheading">
                            <h2>Past Topics:</h2>
                        </div>
                        <ul class="at-projectinfotopics">
                            <li>
                                <i class="icon-arrow-right"></i>
                                <span>Lorem ipsum dolor sit amet, consectetur</span>
                            </li>
                            <li>
                                <i class="icon-arrow-right"></i>
                                <span>Duis aute irure dolor in reprehenderit</span>
                            </li>
                            <li>
                                <i class="icon-arrow-right"></i>
                                <span>Accusantium doloremque laudantium</span>
                            </li>
                            <li>
                                <i class="icon-arrow-right"></i>
                                <span>Totam rem aperiam</span>
                            </li>
                            <li>
                                <i class="icon-arrow-right"></i>
                                <span>Quae ab illo inventore veritatis et quasi</span>
                            </li>
                            <li>
                                <i class="icon-arrow-right"></i>
                                <span>Architecto beatae vitae dicta sunt explicabo</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
            Program Information End
        *************************************-->
    </main>
@endsection
@endsection

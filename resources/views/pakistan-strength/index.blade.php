
<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pakistan Strength and Conditioning Association</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('pakistan-strength.layouts.css')
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!--************************************
        Wrapper Start
*************************************-->
<div id="at-wrapper" class="at-wrapper at-haslayout">
    <!--************************************
            Header Start
    *************************************-->
    @include('pakistan-strength.layouts.header')
    <!--************************************
            Header End
    *************************************-->
    <!--************************************
            Home Slider Start
    *************************************-->
    <div id="at-homeslider" class="at-homeslider owl-carousel owl-theme at-haslayout">
        <div class="item">
            <figure class="at-sliderimage">
                <img src="/images/banner-img.jpg" alt="image description">
                <figcaption class="at-slidercontent">
                    <strong class="at-logo"><a href="javascript:void(0);"><img src="/images/logo.png" alt="company logo"></a></strong>
                    <h1>Pakistan <em>Strength</em></h1>
                    <strong class="at-bannertext">&amp; Conditioning Association</strong>
                    <!-- <a class="at-skewstyle at-btn at-joinbtn" href="javascription:void(0);"><span>Join Now!</span></a> -->
                </figcaption>
            </figure>
        </div>
        <div class="item">
            <figure class="at-sliderimage">
                <img src="images/banner-img.jpg" alt="image description">
                <figcaption class="at-slidercontent">
                    <strong class="at-logo"><a href="javascript:void(0);"><img src="/images/logo.png" alt="company logo"></a></strong>
                    <h1>Pakistan <em>Strength</em></h1>
                    <strong class="at-bannertext">&amp; Conditioning Association</strong>
                    <!-- <a class="at-skewstyle at-btn at-joinbtn" href="javascription:void(0);"><span>Join Now!</span></a> -->
                </figcaption>
            </figure>
        </div>
        <div class="item">
            <figure class="at-sliderimage">
                <img src="images/banner-img.jpg" alt="image description">
                <figcaption class="at-slidercontent">
                    <strong class="at-logo"><a href="javascript:void(0);"><img src="/images/logo.png" alt="company logo"></a></strong>
                    <h1>Pakistan <em>Strength</em></h1>
                    <strong class="at-bannertext">&amp; Conditioning Association</strong>
                    <!-- <a class="at-skewstyle at-btn at-joinbtn" href="javascription:void(0);"><span>Join Now!</span></a> -->
                </figcaption>
            </figure>
        </div>
    </div>
    <!--************************************
            Home Slider End
    *************************************-->
    <!--************************************
            Main Start
    *************************************-->
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                    Counter Start
        *************************************-->
        <section class="at-counter at-haslayout">
            <div class="container">
                <div class="row">
                    <div id="at-counters" class="at-counters">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="at-counterbox">
                                <h3>People Trained</h3>
                                <strong class="at-statistic-count" data-from="0" data-to="1000" data-speed="5000" data-refresh-interval="50">1000+</strong>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="at-counterbox">
                                <h3>Cities Covered</h3>
                                <strong class="at-statistic-count" data-from="0" data-to="27" data-speed="5000" data-refresh-interval="50">27+</strong>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="at-counterbox">
                                <h3>Offered Courses</h3>
                                <strong class="at-statistic-count" data-from="0" data-to="30" data-speed="5000" data-refresh-interval="50">30+</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
                    Counter End
        *************************************-->
        <!--************************************
                    Our Mission Start
        *************************************-->
        <section class="at-ourmission at-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 pull-right">
                        <figure class="at-ourmission-img">
                            <img src="images/ourmission-img.png" alt="image description">
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 pull-left">
                        <div class="at-ourmissioncontent">
                            <div class="at-sectionheading">
                                <h2>our mission</h2>
                            </div>
                            <div class="at-description">
                                @if(isset($mission))
                                <p>{{$mission->mission}}</p>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
                    Our Mission End
        *************************************-->
        <!--************************************
                    Courses Start
        *************************************-->
        <section class="at-courses at-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="at-sectionheading">
                            <h2>Courses</h2>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="at-coursebox">
                            <figure class="at-courseimg">
                                <a href="courses.html"><img src="images/courses/01.jpg" alt="images description"></a>
                            </figure>
                            <div class="at-coursecontent">
                                <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 1</span></a>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                                <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="at-coursebox">
                            <figure class="at-courseimg">
                                <a href="courses.html"><img src="images/courses/02.jpg" alt="images description"></a>
                            </figure>
                            <div class="at-coursecontent">
                                <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 2</span></a>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                                <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="at-coursebox">
                            <figure class="at-courseimg">
                                <a href="courses.html"><img src="images/courses/03.jpg" alt="images description"></a>
                            </figure>
                            <div class="at-coursecontent">
                                <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 3</span></a>
                                <div class="at-description">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                </div>
                                <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
                    Courses End
        *************************************-->
        <!--************************************
                    Gallery Start
        *************************************-->
        <section class="at-gallery at-haslayout">
            <div class="row">
                <div class="col-xs-12">
                    <div class="at-sectionheading">
                        <h2>Gallery</h2>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="at-galleryarea">
                        <ul class="nav nav-tabs at-navtabs" role="tablist">
                            <li role="presentation" class="active"><a href="#event1" class="at-skewstyle" aria-controls="event1" role="tab" data-toggle="tab"><span>event 1</span></a></li>
                            <li role="presentation"><a href="#event2" class="at-skewstyle" aria-controls="event2" role="tab" data-toggle="tab"><span>event 2</span></a></li>
                            <li role="presentation"><a href="#event3" class="at-skewstyle" aria-controls="event3" role="tab" data-toggle="tab"><span>event 3</span></a></li>
                        </ul>
                        <div class="tab-content at-tabcontent">
                            <div role="tabpanel" class="tab-pane active" id="event1">
                                <div class="at-gallerycontentholder">
                                    <div class="at-galleryimages">
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img01.jpg" alt="image description">
                                            </figure>
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img02.jpg" alt="image description">
                                            </figure>
                                        </div>
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img03.jpg" alt="image description">
                                            </figure>
                                        </div>
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img04.jpg" alt="image description">
                                            </figure>
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img05.jpg" alt="image description">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="at-gallerycontent">
                                        <h3>Best Fitness Event in Pakistan</h3>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor amet, conctetur adipiscing elit facilisis. </p>
                                        </div>
                                        <a href="gallery.html" class="at-skewstyle at-btn at-btnreadmore"><span><i class="icon-arrow-right"></i></span></a>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="event2">
                                <div class="at-gallerycontentholder">
                                    <div class="at-galleryimages">
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img01.jpg" alt="image description">
                                            </figure>
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img02.jpg" alt="image description">
                                            </figure>
                                        </div>
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img03.jpg" alt="image description">
                                            </figure>
                                        </div>
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img04.jpg" alt="image description">
                                            </figure>
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img05.jpg" alt="image description">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="at-gallerycontent">
                                        <h3>Best Fitness Event in Pakistan</h3>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor amet, conctetur adipiscing elit facilisis. </p>
                                        </div>
                                        <a href="gallery.html" class="at-skewstyle at-btn at-btnreadmore"><span><i class="icon-arrow-right"></i></span></a>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="event3">
                                <div class="at-gallerycontentholder">
                                    <div class="at-galleryimages">
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img01.jpg" alt="image description">
                                            </figure>
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img02.jpg" alt="image description">
                                            </figure>
                                        </div>
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img03.jpg" alt="image description">
                                            </figure>
                                        </div>
                                        <div class="at-galleryimagebox">
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img04.jpg" alt="image description">
                                            </figure>
                                            <figure class="at-galleryimg">
                                                <img src="images/gallery/img05.jpg" alt="image description">
                                            </figure>
                                        </div>
                                    </div>
                                    <div class="at-gallerycontent">
                                        <h3>Best Fitness Event in Pakistan</h3>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor amet, conctetur adipiscing elit facilisis. </p>
                                        </div>
                                        <a href="gallery.html" class="at-skewstyle at-btn at-btnreadmore"><span><i class="icon-arrow-right"></i></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
                    Gallery End
        *************************************-->
        <!--************************************
                    Contact Start
        *************************************-->
        <section class="at-contact at-haslayout">
            <div class="at-sectionhead">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="at-sectionheading">
                                <h2>Contact</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="at-contantarea">
                <div class="container">
                    <div class="row">
                        <div class="at-contantbox">
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <form class="at-formtheme at-contactform">
                                    <fieldset>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="NAME">
                                        </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="SUBJECT"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="at-skewstyle at-btn"><span>submit</span></button>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6">
                                <figure class="at-map border-left">
                                    <img src="images/map-img2.png" alt="image description">
                                    <figcaption class="at-mapmarker">
                                        <div class="at-tooltip">
                                            <i class="fa fa-circle"></i>
                                            <div class="at-top">
                                                <h3>Pakistan Office</h3>
                                                <p>24 B , Lahore. #923214567890</p>
                                                <i></i>
                                            </div>
                                        </div>
                                        <div class="at-tooltip at-tooltip2">
                                            <i class="fa fa-circle"></i>
                                            <div class="at-top">
                                                <h3>Pakistan Office</h3>
                                                <p>24 B , Lahore. #923214567890</p>
                                                <i></i>
                                            </div>
                                        </div>
                                        <div class="at-tooltip at-tooltip3">
                                            <i class="fa fa-circle"></i>
                                            <div class="at-top">
                                                <h3>Pakistan Office</h3>
                                                <p>24 B , Lahore. #923214567890</p>
                                                <i></i>
                                            </div>
                                        </div>
                                        <div class="at-tooltip at-tooltip4">
                                            <i class="fa fa-circle"></i>
                                            <div class="at-top">
                                                <h3>Pakistan Office</h3>
                                                <p>24 B , Lahore. #923214567890</p>
                                                <i></i>
                                            </div>
                                        </div>
                                        <div class="at-tooltip at-tooltip5">
                                            <i class="fa fa-circle"></i>
                                            <div class="at-top">
                                                <h3>Pakistan Office</h3>
                                                <p>24 B , Lahore. #923214567890</p>
                                                <i></i>
                                            </div>
                                        </div>
                                        <div class="at-tooltip at-tooltip6">
                                            <i class="fa fa-circle"></i>
                                            <div class="at-top">
                                                <h3>Pakistan Office</h3>
                                                <p>24 B , Lahore. #923214567890</p>
                                                <i></i>
                                            </div>
                                        </div>
                                        <h3 class="at-mapheading">Existence in<span>Pakistan</span></h3>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--************************************
                    Contact End
        *************************************-->
    </main>
    <!--************************************
            Main End
    *************************************-->
    <!--************************************
            Footer Start
    *************************************-->
   @include('pakistan-strength.layouts.footer')
    <!--************************************
            Footer End
    *************************************-->
</div>
<!--************************************
        Wrapper End
*************************************-->
@include('pakistan-strength.layouts.js')
</body>
</html>

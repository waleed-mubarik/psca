@extends('pakistan-strength.layouts.master')
@section('title',"Excellence Center's Services")
@section('headings')
            <h1>{{$excellence_center_service->page_name}}</h1>
        <strong class="at-bannertext"></strong>
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
            Excellence Services Start
        *************************************-->
        <div class="at-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="at-excellenceservices">
                            <figure class="at-excellenceserviceimg"><img src="{{asset('uploads/service-image/'.$excellence_center_service->image)}}" alt="image description"></figure>
                            <div class="at-excellenceservicecontent">
                                <div class="at-title">
                                    <div class="at-programinfoheading">
                                        <h2>Title</h2>
                                    </div>
                                    <div class="at-description">
                                        <p>{!!$excellence_center_service->title!!}</p>
                                    </div>
                                </div>
                                <div class="at-vision">
                                    <div class="at-programinfoheading">
                                        <h2>Description</h2>
                                    </div>
                                    <div class="at-description">
                                        <p>{!! $excellence_center_service->description !!}</p>
                                    </div>
                                </div>
                                <div class="at-mission">
                                    <div class="at-programinfoheading">
                                        <h2>Instructions</h2>
                                    </div>
                                    <div class="at-description">
                                       {!! $excellence_center_service->instruction !!}
                                    </div>
                                </div>
                            </div>
                            <div class="at-goalarea">
                                <h2>Goal</h2>
                                <div class="at-description">
                                    <p>{!! $excellence_center_service->goal !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
            Excellence Services End
        *************************************-->
    </main>
@endsection
@endsection

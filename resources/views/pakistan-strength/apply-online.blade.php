@extends('pakistan-strength.layouts.master')
@section('title','Apply Online')
@section('headings')
    <h1>Apply <em>Online</em></h1>--}}
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                Apply Online Start
        *************************************-->
        <div class="at-haslayout">
            <div class="at-applyonline">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="at-description">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <span>labore et dolore magna aliqua. Ut enim ad minim veniam commodo consequat.</span></p>
                            </div>
                            <form class="at-formtheme at-formapplyonline">
                                <fieldset>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <input type="text" name="name" class="form-control" placeholder="NAME">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <input type="email" name="email" class="form-control" placeholder="Email">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <input type="text" name="text" class="form-control" placeholder="Phone no.">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <input type="text" name="text" class="form-control" placeholder="City">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <input type="text" name="text" class="form-control" placeholder="Qualification/ Achievements">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="Comment"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <div class="form-group at-marginbottomzero">
                                                <button class="at-skewstyle at-btn"><span>Apply Now!</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Apply Online End
        *************************************-->
    </main>
@endsection
@endsection


@extends('pakistan-strength.layouts.master')
@section('title','Upcoming Courses')
@section('headings')
    <h1>Upcoming <em>Courses</em></h1>--}}
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                Upcoming Courses Start
        *************************************-->
        <div class="at-haslayout">
            <div class="at-upcomingcouses">
                <div class="container">
                    <div class="row">
                        <div class="at-upcomingcousesholder">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="{{route('courses')}}"><img src="images/courses/01.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="{{route('courses')}}" class="at-skewstyle at-btn at-coursebtn"><span>Course 1</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="{{route('courses')}}" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/02.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 2</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/03.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 3</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/02.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 4</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/03.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 5</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/01.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 6</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/03.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 7</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/01.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 8</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="courses.html"><img src="images/courses/02.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <a href="courses.html" class="at-skewstyle at-btn at-coursebtn"><span>Course 9</span></a>
                                        <div class="at-description">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
                                        </div>
                                        <a href="courses.html" class="at-readmorebtn"><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Upcoming Courses End
        *************************************-->
    </main>
@endsection
@endsection


@extends('pakistan-strength.layouts.master')
@section('headings')
    <h1>Courses</h1>
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                    Courses Start
        *************************************-->
        <div class="at-courses at-allcourses at-haslayout">
            <div class="container">
                <div class="row">
                    <div class="at-coursesarea">
                        <div class="col-xs-12">
                            <form class="at-formtheme at-courseform">
                                <fieldset>
                                    <div class="form-group">
                                        <label>Cities:</label>
                                        <div class="at-select">
                                            <select class="form-control">
                                                <option>Lahore</option>
                                                <option>Islamabad</option>
                                                <option>Karachi</option>
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-xs-12">
                            <div class="at-coursebox">
                                <figure class="at-courseimg">
                                    <img src="images/courses/04.jpg" alt="images description">
                                </figure>
                                <div class="at-coursecontent">
                                    <div class="at-coursehead">
                                        <h2>Course Name</h2>
                                        <span>Rs.<strong>2500</strong></span>
                                    </div>
                                    <ul class="at-courseinfo">
                                        <li><span>Duration:</span><strong>6 Month</strong></li>
                                        <li><span>Location:</span><address>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit, sed tempor incididunt.</address></li>
                                        <li><span>Next Batch</span><em>23-02-2019</em></li>
                                    </ul>
                                    <a href="javascript:void(0);" class="at-skewstyle at-btn at-enrollbtn" data-toggle="modal" data-target="#enrollmodal">
                                        <span>Enroll</span>
                                    </a>
                                </div>
                            </div>
                            <div class="at-coursebox">
                                <figure class="at-courseimg">
                                    <img src="images/courses/05.jpg" alt="images description">
                                </figure>
                                <div class="at-coursecontent">
                                    <div class="at-coursehead">
                                        <h2>Course Name</h2>
                                        <span>Rs.<strong>2500</strong></span>
                                    </div>
                                    <ul class="at-courseinfo">
                                        <li><span>Duration:</span><strong>6 Month</strong></li>
                                        <li><span>Location:</span><address>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit, sed tempor incididunt.</address></li>
                                        <li><span>Next Batch</span><em>23-02-2019</em></li>
                                    </ul>
                                    <a href="javascript:void(0);" class="at-skewstyle at-btn at-enrollbtn"><span>Enroll</span></a>
                                </div>
                            </div>
                            <div class="at-coursebox">
                                <figure class="at-courseimg">
                                    <img src="images/courses/06.jpg" alt="images description">
                                </figure>
                                <div class="at-coursecontent">
                                    <div class="at-coursehead">
                                        <h2>Course Name</h2>
                                        <span>Rs.<strong>2500</strong></span>
                                    </div>
                                    <ul class="at-courseinfo">
                                        <li><span>Duration:</span><strong>6 Month</strong></li>
                                        <li><span>Location:</span><address>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit, sed tempor incididunt.</address></li>
                                        <li><span>Next Batch</span><em>23-02-2019</em></li>
                                    </ul>
                                    <a href="javascript:void(0);" class="at-skewstyle at-btn at-enrollbtn"><span>Enroll</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                    Courses End
        *************************************-->
    </main>
@endsection
@endsection

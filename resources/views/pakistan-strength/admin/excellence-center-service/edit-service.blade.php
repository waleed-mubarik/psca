@extends('pakistan-strength.admin.layouts.master')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Edit Excellence Center's Service</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Excellence Center's Service</li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">Other Sample form</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{route('update-service',$excellence_center_service->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-body">
                                <h3 class="card-title">Excellence Center's Service</h3>
                                <hr>
                                <div class="row p-t-20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Service Name</label>
                                            <input type="text" name="page_name" value="{{$excellence_center_service->page_name}}" id="name" class="form-control" >
                                            <small class="form-control-feedback">  </small> </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Title</label>
                                            <textarea class="at-tinymceeditor" name="title">{{$excellence_center_service->title}}</textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description</label>
                                            <textarea class="at-tinymceeditor" name="description">{{$excellence_center_service->description}}</textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Instructions</label>
                                            <textarea class="at-tinymceeditor" name="instruction">{!!  $excellence_center_service->instruction!!}</textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
                                    {{--                                    <!--/span-->--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Goal</label>
                                            <textarea class="at-tinymceeditor" name="goal">{{$excellence_center_service->goal}}</textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
                                    {{--                                    <!--/span-->--}}
                                    {{--                                </div>--}}
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">File Upload1</h4>
                                                <img src="{{asset('uploads/service-image/'.$excellence_center_service->image)}}">
                                                <input type="file" name="image"  id="input-file-now" class="dropify" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <!--/span-->

                                </div>
                            </div>
                        </form>
                        <div class="form-actions pull-right">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                            <form action="{{route('delete-service',$excellence_center_service->id)}}" method="post">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-inverse">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('script')
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
        if (jQuery('.at-tinymceeditor').length > 0) {
            tinymce.init({
                selector: 'textarea.at-tinymceeditor',
                height: 250,
                theme: 'modern',
                plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
            });
        }

    </script>
@endsection
@endsection


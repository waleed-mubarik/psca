@extends('pakistan-strength.admin.layouts.master')
@section('title','All Excellence Center Service')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Excellence Center's Services</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Excellence Center's Service</li>
                    <li class="breadcrumb-item active">All Services</li>
                </ol>
            </div>
        </div>
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover nowrap at-servicestable">
                            <thead>
                            <tr>
                                <th colspan="2">Title</th>
                                <th>Description</th>
                                <th>Instructions</th>
                                <th>Goal</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($all_services))
                                @foreach($all_services as $service)
                                    <tr>
                                        <td><span class="round"><img src="{{asset('uploads/service-image/'.$service->image)}}" alt="user" width="50"></span></td>
                                        <td>
                                            <h6>{{$service->name}}</h6><small class="text-muted">{{ucwords($service->page_name)}}</small></td>
                                        {{--                        <td><span class="label label-success">Low</span></td>--}}
                                        <td>{!!  \Illuminate\Support\Str::words($service->description,3) !!}</td>
                                        <td>{!!  \Illuminate\Support\Str::limit($service->instruction) !!}</td>
                                        <td>{!! \Illuminate\Support\Str::words($service->goal,3)  !!}</td>
                                        <td>
                                            <a href="javascript:void(0)" class="text-inverse p-r-10" title="" data-original-title="View" data-toggle="modal" data-target="#{{$service->id}}"><i class=" ti-eye"></i></a>
                                            <a href="{{route('edit-service',$service->id)}}" class="text-inverse" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a>
                                            <form class="at-formservice" action="{{route('delete-service',$service->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="text-inverse at-servicedelete" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button>
                                            </form>
                                        </td>
                                        <div class="modal fade bs-example-modal-lg" id="{{$service->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Detail  </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-12 m-b-40"> <img src="{{asset('uploads/service-image/'.$service->image)}}" class="img-responsive thumbnail"> </div>
                                                            <div class="col-md-12 m-b-20"><h4 class="card-title" id="1">Title</h4> {!! $service->title!!}</div>
                                                            <div class="col-md-12 m-b-20"><h4 class="card-title" id="1">Description</h4> {!! $service->description!!}</div>
                                                            <div class="col-md-12 m-b-20"><h4 class="card-title" id="1">Instruction</h4> {!! $service->instruction!!}</div>
                                                            <div class="col-md-12 m-b-20"><h4 class="card-title" id="1">Goals</h4> {!! $service->goal!!}</div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- sample modal content -->
    </div>
@endsection




@extends('pakistan-strength.admin.layouts.master')
@section('title','Add Excellence Center Service')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add Excellence Center's Service</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Excellence Center's Service</li>
                    <li class="breadcrumb-item active">Add</li>
                </ol>
            </div>
        </div>
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">Excellence Center's Service</h4>
                    </div>
                        <form action="{{route('store-service')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                            <div class="form-body">
                                <div class="row p-t-20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Service Name</label>
                                            <br>
                                            @if($errors->any())
                                                <div class="alert alert-danger">
                                                {{$errors->first('page_name')}}
                                                </div>
                                            @endif
                                            <input type="text" name="page_name" id="name" class="form-control" >
                                            <small class="form-control-feedback">  </small> </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Title</label>
                                            <br>
                                            @if($errors->any())
                                                <div class="alert alert-danger">
                                                    {{$errors->first('title')}}
                                                </div>
                                            @endif
                                            <textarea class="at-tinymceeditor" name="title"></textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Description</label>
                                            <br>
                                            @if($errors->any())
                                                <div class="alert alert-danger">
                                                    {{$errors->first('description')}}
                                                </div>
                                            @endif
                                            <textarea class="at-tinymceeditor" name="description"></textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
                                <!--/row-->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Instructions</label>
                                        <br>
                                        @if($errors->any())
                                            <div class="alert alert-danger">
                                                {{$errors->first('instruction')}}
                                            </div>
                                        @endif
                                        <textarea class="at-tinymceeditor" name="instruction"></textarea>
                                        <small class="form-control-feedback"> </small>
                                    </div>
                                </div>
{{--                                    <!--/span-->--}}
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Goal</label>
                                            <br>
                                            @if($errors->any())
                                                <div class="alert alert-danger">
                                                    {{$errors->first('goal')}}
                                                </div>
                                            @endif
                                            <textarea class="at-tinymceeditor" name="goal"></textarea>
                                            <small class="form-control-feedback"> </small>
                                        </div>
                                    </div>
{{--                                    <!--/span-->--}}
{{--                                </div>--}}
                                    <div class="col-md-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Image</h4>
                                                <br>
                                                @if($errors->any())
                                                    <div class="alert alert-danger">
                                                        {{$errors->first('image')}}
                                                    </div>
                                                @endif
                                                <label for="input-file-now">Your so fresh input file — Default version</label>
                                                <input type="file" name="image" id="input-file-now" class="dropify" />
                                            </div>
                                        </div>
                                    </div>
                                    <!--/row-->
                                    <!--/span-->

                                </div>
                                <div class="form-actions pull-right">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-inverse">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove: 'Supprimer',
                    error: 'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element) {
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element) {
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element) {
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e) {
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
        if (jQuery('.at-tinymceeditor').length > 0) {
            tinymce.init({
                selector: 'textarea.at-tinymceeditor',
                height: 250,
                theme: 'modern',
                plugins: ['advlist autolink lists link image charmap print preview hr anchor pagebreak', 'searchreplace wordcount visualblocks visualchars code fullscreen', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help'],
                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                toolbar2: 'print preview media | forecolor backcolor emoticons | codesample help',
                image_advtab: true,
            });
        }

    </script>
@endsection



@extends('pakistan-strength.admin.layouts.master')
@section('title','Add Student')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add Student</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Program Alumni</li>
                    <li class="breadcrumb-item active">Add Student</li>
                </ol>
            </div>
        </div>
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Person Info</h4>
                        </div>
                        <div class="card-body">
                            <form action="#"  id="submit-image" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-body">
                                    <div class="row p-t-20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">First Name</label>
                                                <input type="text" name="name" id="name" class="form-control" >
                                                <small class="form-control-feedback "> {{$errors->first('name')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">

                                            <label class="control-label">Email</label>
                                            <input type="text" name="email" id="email" class="form-control form-control-danger" placeholder="">
                                            <small class="form-control-feedback "> {{$errors->first('email')}} </small> </div>

                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-success">
                                                <label class="control-label">Gender</label>
                                                <select name="gender" id="gender" class="form-control custom-select">
                                                    <option value="" selected disabled="">Select Gender</option>
                                                    <option value="male">Male</option>
                                                    <option value="female">Female</option>
                                                </select>
                                                <small class="form-control-feedback "> {{$errors->first('gender')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Batch</label>
                                                <select name="batch_id" id="batch_id" class="form-control">
                                                    <option selected disabled>Select Batch</option>
                                                    @foreach($batches as $batch)
                                                        <option VALUE="{{$batch->id}}">
                                                            {{$batch->batch_name}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <small class="form-control-feedback "> {{$errors->first('batch_name')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-success">
                                                <label class="control-label">Country</label>
                                                <select name="country" class="form-control custom-select">
                                                    <option value="Pakistan">Pakistan</option>
                                                    <option value="India">India</option>
                                                    <option value="USA">USA</option>
                                                </select>
                                                <small class="form-control-feedback "> {{$errors->first('country')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">City</label>
                                                <input type="text" name="city" id="city" class="form-control" >
                                                <small class="form-control-feedback "> {{$errors->first('city')}} </small> </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Contact</label>
                                                <input type="number" name="contact" id="sport" class="form-control" >
                                                <small class="form-control-feedback "> {{$errors->first('contact')}} </small> </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Sport</label>
                                                <input type="text" name="sport" id="sport" class="form-control" >
                                                <small class="form-control-feedback "> {{$errors->first('sport')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <textarea name="description"></textarea>
                                                <small class="form-control-feedback "> {{$errors->first('description')}} </small> </div>
                                        </div>
                                    </div>
                                    <div class="alert alert-danger" style="display:none"></div>
                                    <div class="alert alert-success" style="display:none"></div>
{{--                                    <div class="col-md-12">--}}
{{--                                        <div class="card">--}}
{{--                                            <div class="card-body">--}}
{{--                                                <h4 class="card-title">File Upload1</h4>--}}
{{--                                                <label for="input-file-now">Your so fresh input file — Default version</label>--}}
{{--                                                <input type="file" name="avatar" id="input-file-now" class="dropify" />--}}
{{--                                                <small class="form-control-feedback alert-danger"> {{$errors->first('avatar')}} </small> </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                    <div class="at-uploadimg">
                                        <label for="imgInp">Attach Image
                                            <input type='file' name="avatar" id="imgInp" />
                                            <img id="my-image" src="#" />
                                        </label>
                                    </div>
                                </div>


                                <div class="form-actions pull-right">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                </div>
                            </form>
                            <button id="use" class="form">Upload</button>
                            <img id="result" src="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
        @section('script')
        @section('script')
            <script>
                $(document).ready(function() {
                    var data;
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('#my-image').attr('src', e.target.result);
                                var resize = new Croppie($('#my-image')[0], {
                                    viewport: { width: 450, height: 250 },
                                    boundary: { width: 700, height: 400 },
                                    showZoomer: true,
                                    enableResize: false,
                                    enableOrientation: true
                                });
                                $('#use').fadeIn();
                                $('#use').on('click', function(event) {
                                    event.preventDefault();
                                    resize.result('base64').then(function(dataImg) {
                                        data =  {image: dataImg} ;
                                        // use ajax to send data to php
                                        $('#result').attr('src', dataImg);
                                    })
                                })
                                $('#submit-image').on('submit',function (event) {
                                    event.preventDefault();
                                    var url = '{{url('/admin/store-student')}}';
                                    var name = $("#name").val();
                                    var gender = $("#gender").val();
                                    var batch_id = $("#batch_id").val();
                                    var CSRF_TOKEN = "{{csrf_token()}}"
                                    $.ajax({
                                        url:url,
                                        method:'POST',
                                        type:'json',
                                        data:{data:data,
                                            name:name,
                                            gender:gender,
                                            batch_id:batch_id,
                                            _token:CSRF_TOKEN},
                                        success: function(data){
                                            $.each(data.errors, function(key, value){
                                                $('.alert-success').remove();
                                                $('.alert-danger').show();
                                                $('.alert-danger').append('<p>'+value+'</p>');
                                            });
                                            $(data.success,function () {
                                                $('.alert-danger').remove();
                                                $('.alert-success').show();
                                                $('.alert-success').append('<p>'+data.success+'</p>');
                                            })

                                        },

                                    });
                                    $("#result").remove();
                                    $("#use").remove();
                                    $("#imgInp").show();
                                    $("#submit-image").trigger('reset');
                                })

                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }

                    $("#imgInp").change(function() {
                        readURL(this);
                    });

                });
            </script>
@endsection




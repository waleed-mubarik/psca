@extends('pakistan-strength.admin.layouts.master')
@section('title','Edit Profile')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Edit Student Profile</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Program Alumni</li>
                    <li class="breadcrumb-item active">Edit Student</li>
                </ol>
            </div>
        </div>
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Other Sample form</h4>
                        </div>
                        <div class="card-body">
                            <form action="#" id="submit-image" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                <input type="hidden" id="id" value="{{$student->id}}">
                                <div class="form-body">
                                    <h3 class="card-title">Person Info</h3>
                                    <hr>
                                    <div class="row p-t-20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">First Name</label>
                                                <input type="text" name="name" value="{{$student->name}}" id="name" class="form-control" >
                                                <small class="form-control-feedback alert-danger"> {{$errors->first('name')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">

                                            <label class="control-label">Email</label>
                                            <input type="text" name="email" id="email" class="form-control form-control-danger" placeholder="">
                                            <small class="form-control-feedback alert-danger"> {{$errors->first('email')}} </small> </div>

                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group has-success">
                                                <label class="control-label">Gender</label>
                                                <select name="gender" id="gender" class="form-control custom-select">
                                                    @if($student->gender == 'male')
                                                        <option value="{{$student->gender}}" selected>{{ucfirst($student->gender)}}</option>
                                                        <option value="female">Female</option>
                                                    @elseif($student->gender == 'female')
                                                        <option value="male">Male</option>
                                                        <option value="{{$student->gender}}" selected>{{ucfirst($student->gender)}}</option>
                                                    @endif
                                                </select>
                                                <small class="form-control-feedback alert-danger"> {{$errors->first('gender')}} </small> </div>
                                        </div>
                                        <!--/span-->
                                    {{--                                        <div class="col-md-6">--}}
                                    {{--                                            <div class="form-group">--}}
                                    {{--                                                <label class="control-label">Batch</label>--}}
                                    {{--                                                <select name="batch_id" class="form-control">--}}
                                    {{--                                                    <option selected disabled>Select Batch</option>--}}
                                    {{--                                                    @foreach($batches as $batch)--}}
                                    {{--                                                        <option VALUE="{{$batch->id}}">--}}
                                    {{--                                                            {{$batch->batch_name}}--}}
                                    {{--                                                        </option>--}}
                                    {{--                                                    @endforeach--}}
                                    {{--                                                </select>--}}
                                    {{--                                                <small class="form-control-feedback alert-danger"> {{$errors->first('batch_name')}} </small> </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <!--/span-->--}}
                                    {{--                                    </div>--}}
                                    <!--/row-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group has-success">
                                                    <label class="control-label">Country</label>
                                                    <select name="country" class="form-control custom-select">
                                                        <option value="Pakistan">Pakistan</option>
                                                        <option value="India">India</option>
                                                        <option value="USA">USA</option>
                                                    </select>
                                                    <small class="form-control-feedback alert-danger"> {{$errors->first('country')}} </small> </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">City</label>
                                                    <input type="text" name="city" id="city" class="form-control" >
                                                    <small class="form-control-feedback alert-danger"> {{$errors->first('city')}} </small> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Contact</label>
                                                    <input type="number" name="contact" id="sport" class="form-control" >
                                                    <small class="form-control-feedback alert-danger"> {{$errors->first('contact')}} </small> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Sport</label>
                                                    <input type="text" name="sport" id="sport" class="form-control" >
                                                    <small class="form-control-feedback alert-danger"> {{$errors->first('sport')}} </small> </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Description</label>
                                                    <textarea name="description"></textarea>
                                                    <small class="form-control-feedback alert-danger"> {{$errors->first('description')}} </small> </div>
                                            </div>
                                        </div>
                                                                            <div class="col-md-12">
                                                                                <div class="card">
                                                                                    <div class="card-body">
                                                                                        <h4 class="card-title">Profile Picture</h4>
                                                                                       <img id="student-profile" src="{{asset('uploads/students/avatars/'.$student->avatar)}}">
                                                                                        <small class="form-control-feedback alert-danger"> {{$errors->first('avatar')}} </small> </div>
                                                                                </div>
                                                                            </div>
                                        <input type='file' id="imgInp" />
                                        <img id="my-image" src="#" />
                                    </div>

                                    <div class="form-actions pull-right">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    </div>
                                </div>
                            </form>
                            <div class="alert alert-success" style="display:none"></div>
                            <button id="use" class="form">Upload</button>
                            <img id="result" src="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @endsection
        @section('script')
            <script>
                $(document).ready(function() {
                    var data;
                    function readURL(input) {
                        if (input.files && input.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                $('#my-image').attr('src', e.target.result);
                                var resize = new Croppie($('#my-image')[0], {
                                    viewport: { width: 450, height: 250 },
                                    boundary: { width: 700, height: 400 },
                                    showZoomer: true,
                                    enableResize: false,
                                    enableOrientation: true
                                });
                                $('#use').fadeIn();
                                $('#use').on('click', function(event) {
                                    event.preventDefault();
                                    resize.result('base64').then(function(dataImg) {
                                        data =  {image: dataImg} ;
                                        // use ajax to send data to php
                                        $('#result').attr('src', dataImg);
                                    })
                                })
                                $('#submit-image').on('submit',function (event) {
                                    console.log(data)
                                    event.preventDefault();
                                    var id = $("#id").val();
                                    var url = '/admin/update-student/'+id;
                                    var name = $("#name").val();
                                    var gender = $("#gender").val();
                                    var CSRF_TOKEN = "{{csrf_token()}}"
                                    $.ajax({
                                        url:url,
                                        method:'PUT',
                                        type:'json',
                                        data:{data:data,name:name,gender:gender,_token:CSRF_TOKEN},
                                        success:function(data) {
                                            $(data.success,function () {
                                                $("#result").remove()
                                                $('.alert-success').show();
                                                $('.alert-success').append('<p>'+data.success+'</p>');
                                            })

                                        },

                                    });

                                })

                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }

                    $("#imgInp").change(function() {
                        readURL(this);
                    });


                });
            </script>
@endsection



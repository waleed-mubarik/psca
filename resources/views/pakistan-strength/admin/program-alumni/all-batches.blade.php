@extends('pakistan-strength.admin.layouts.master')
@section('title','All Batches')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">All Batches</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Program Alumni</li>
                    <li class="breadcrumb-item active">All Students</li>
                </ol>
            </div>
        </div>
        <div class="container-fluid">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover earning-box">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($batches))
                                @foreach($batches as $key => $batch)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>
                                            <h6>{{$batch->batch_name}}</h6><small class="text-muted">Web Designer</small></td>
                                        <td>
                                            <a href="{{route('batch-students',$batch->id)}}" class="text-inverse p-r-10" title="" data-original-title="View"><i class=" ti-eye"></i></a>
                                            <a href="" class="text-inverse p-r-10" data-toggle="tooltip" title="" data-original-title="Edit"><i class="ti-marker-alt"></i></a>
                                            <form action="{{route('delete-batch',$batch->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <button onclick="return confirm('are you sure to delete batch and all corresponding students ?')" class="text-inverse" title="" data-toggle="tooltip" data-original-title="Delete"><i class="ti-trash"></i></button>
                                            </form>
                                        </td>
                                        <div class="modal fade bs-example-modal-lg" id="{{$batch->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Detail  </h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-md-4"> <img src="{{asset('uploads/avatars/'.$batch->avatar)}}" class="img-responsive thumbnail m-r-15"> </div>
                                                            <div class="col-md-8"> {{$batch->description}}                                          </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                    </tr>
                                @endforeach
                            @endif

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- sample modal content -->
@endsection

@extends('pakistan-strength.layouts.master')
@section('headings')
    <h1>Program Alumni</h1>
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                Program Alumni Start
        *************************************-->
        <div class="at-haslayout">
            <div class="at-programalumni">
                <div class="container">
                    <div class="row">
                        <div class="at-courseholder">
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/01.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/05.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/02.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/03.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/04.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/02.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/03.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/01.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/02.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/03.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/01.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="programdetail.html"><img src="images/courses/04.jpg" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>Ahsan Raza<span>PCSA Level Certified</span></h2>
                                        <a href="programdetail.html" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Program Alumni End
        *************************************-->
    </main>
@endsection
@endsection


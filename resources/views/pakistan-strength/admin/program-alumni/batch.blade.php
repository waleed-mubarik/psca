@extends('pakistan-strength.admin.layouts.master')
@section('title','Add New Batch')
@section('content')
    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Add Batch</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Program Alumni</li>
                    <li class="breadcrumb-item active">Add Batch</li>
                </ol>
            </div>
        </div>
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header">
                        <h4 class="m-b-0 text-white">Create New Batch</h4>
                    </div>
                    <form action="{{route('store-batch')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-body">
                                <div class="row p-t-20">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Batch Name</label>
                                            <br>
                                            @if($errors->any())
                                                <div class="alert alert-danger">
                                                    {{$errors->first('batch_name')}}
                                                </div>
                                            @endif
                                            <input type="text" name="batch_name" id="name" class="form-control" >
                                            <small class="form-control-feedback">  </small> </div>
                                    </div>

                                </div>
                                <div class="form-actions pull-right">
                                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-inverse">Cancel</button>
                                </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection

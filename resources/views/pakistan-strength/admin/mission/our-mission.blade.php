@extends('pakistan-strength.admin.layouts.master')
@section('title','Our Mission')
@section('content')










    <div class="page-wrapper" style="min-height: 352px;">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Our Mission</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>
                    <li class="breadcrumb-item">Our Mission</li>
                    <li class="breadcrumb-item active">Add or Update Mission</li>
                </ol>
            </div>
        </div>
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                        <div class="card-header">
                            <h4 class="m-b-0 text-white">Create Or Update Mission</h4>
                        </div>
                        <div class="card-body">
                            @if(isset($mission))
                                <form action="{{route('store-mission')}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-body">
                                        <div class="row p-t-20">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Description</label>
                                                    <input hidden name="mission_id" value="{{$mission->id}}">
                                                    <textarea name="mission" cols="100px">{{$mission->mission}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions pull-right">
                                            <button type="submit" class="btn btn-success">Update Mission</button>
                                        </div>
                                    </div>
                                </form>
                            @else
                                <form action="{{route('store-mission')}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="row p-t-20">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Description</label>
                                                <textarea  name="mission" cols="100px"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions pull-right">
                                        <button type="submit" class="btn btn-success">Create Mission</button>
                                    </div>
                                </form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection





{{--    <div class="page-wrapper" style="min-height: 352px;">--}}
{{--        <div class="row page-titles">--}}
{{--            <div class="col-md-5 align-self-center">--}}
{{--                <h3 class="text-themecolor">Our Mission</h3>--}}
{{--            </div>--}}
{{--            <div class="col-md-7 align-self-center">--}}
{{--                <ol class="breadcrumb">--}}
{{--                    <li class="breadcrumb-item"><a href="{{route('admin')}}">Home</a></li>--}}
{{--                    <li class="breadcrumb-item">Our Mission</li>--}}
{{--                    <li class="breadcrumb-item active">Add or Update Mission</li>--}}
{{--                </ol>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="container-fluid">--}}
{{--            @if(isset($mission))--}}
{{--            <form action="{{route('store-mission')}}" method="post">--}}
{{--                @csrf--}}
{{--                @method('PUT')--}}
{{--                    <input hidden name="mission_id" value="{{$mission->id}}">--}}
{{--                    <textarea name="mission" cols="100px">{{$mission->mission}}</textarea>--}}
{{--                <button type="submit">Update Mission</button>--}}
{{--            </form>--}}
{{--            @else--}}
{{--            <form action="{{route('store-mission')}}" method="post">--}}
{{--                @csrf--}}
{{--                @method('PUT')--}}
{{--                <textarea  name="mission" cols="100px"></textarea>--}}
{{--                <button type="submit">Create Mission</button>--}}
{{--            </form>--}}
{{--            @endif--}}
{{--    </div>--}}
{{--@endsection--}}

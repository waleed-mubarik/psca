<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <!-- User profile image -->
            {{--<div class="profile-img"> <img src="/assets/images/users/profile.png" alt="user" />
                <!-- this is blinking heartbit-->
                <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
            </div>
            <!-- User profile text-->
            <div class="profile-text">
                @if(\Illuminate\Support\Facades\Auth::check())
                <h5>{{\Illuminate\Support\Facades\Auth::user()->name }}</h5>
                @endif
                <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="mdi mdi-settings"></i></a>
                <a href="app-email.html" class="" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
                <form action="{{route('logout')}}" method="post">
                    @csrf
                <button type="submit" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></button>
                </form>
                <div class="dropdown-menu animated flipInY">
                    <!-- text-->
                    <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                    <!-- text-->
                    <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                    <!-- text-->
                    <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <!-- text-->
                    <div class="dropdown-divider"></div>
                    <!-- text-->
                    <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                    <!-- text-->
                    <div class="dropdown-divider"></div>
                    <!-- text-->
                    <a href="login.html" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                    <!-- text-->
                </div>
            </div>--}}
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-small-cap">PERSONAL</li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Faculty</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('admin')}}">All Faculty Members</a></li>
                        <li><a href="{{route('create-faculty')}}">Add Faculty Member</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Program Alumni</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('all-student')}}">All Student</a></li>
                        <li><a href="{{route('add-batch')}}">Create Batch</a></li>
                        <li><a href="{{route('all-batch')}}">All Batch</a></li>
                        <li><a href="{{route('add-student')}}">Add Student</a></li>
                    </ul>
                </li>

                <li> <a class="has-arrow waves-effect waves-dark" href="{{route('mission')}}" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Our Mission</span></a>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Excellence Center's Service</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('show-all-services')}}">All</a></li>
                        <li><a href="{{route('create-service')}}">Create Excellence Center's Service</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>

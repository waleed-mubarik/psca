<link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
<!-- chartist CSS -->
<link href="{{asset('assets/plugins/chartist-js/dist/chartist.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/chartist-js/dist/chartist-init.css')}}" rel="stylesheet">
<link href="{{asset('assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css')}}" rel="stylesheet">
<!-- morris CSS -->
<link href="{{asset('assets/plugins/morrisjs/morris.css')}}" rel="stylesheet">
<!-- Vector CSS -->
<link href="{{asset('assets/plugins/vectormap/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
<link href="{{asset('assets/plugins/dropify/dist/css/dropify.min.css')}}" rel="stylesheet" />
<link href="{{asset('assets/css/croppie.css')}}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{asset('assets/css/style.css')}}" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="{{asset('assets/css/colors/blue.css')}}" id="theme" rel="stylesheet">

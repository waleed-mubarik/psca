<footer id="at-footer" class="at-footer at-haslayout">
    <div class="at-mainfooter">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="at-col">
                        <figure class="at-footerlog">
                            <a href="index.html"><img src="/images/logo2.png" alt="image description"></a>
                        </figure>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="at-col">
                        <div class="at-sectionheading">
                            <h2>Company</h2>
                        </div>
                        <ul class="at-linklist">
                            <li><a href="index.html">Home</a></li>
                            <li><a href="aboutus.html">About</a></li>
                            <li><a href="gallery.html">Gallery</a></li>
                            <li><a href="courses.html">Coursrs</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="at-col">
                        <div class="at-sectionheading">
                            <h2>Address</h2>
                        </div>
                        <div class="at-description">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed tempor incididunt.</p>
                        </div>
                        <ul class="at-infolist">
                            <li><span>Email:</span><em><a href="mailto:info@pakistanstrength.com"></a>info@pakistanstrength.com</em></li>
                            <li><span>Phone:</span><em>+92 123 4567890</em></li>
                            <li><span>Fax:</span><em>+92 123 4567890</em></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="at-col">
                        <div class="at-sectionheading">
                            <h2>Get in Touch</h2>
                        </div>
                        <ul class="at-socialicons">
                            <li class="at-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                            <li class="at-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                            <li class="at-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="at-footerbar">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <span class="at-copyrights">© Pakistan Strength & Cinditioning Association All rights Resevered.</span>
                </div>
            </div>
        </div>
    </div>
</footer>

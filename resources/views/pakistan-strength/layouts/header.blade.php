<header id="at-header" class="at-header at-innerheader at-haslayout">
    <div class="at-navigationarea">
        <strong class="at-logo"><a href="{{url('/')}}"><img src="/images/logo.png" alt="company logo"></a></strong>
        <a class="at-skewstyle at-btn at-contactbtn" href="javascript:void(0);"><span>Join Now!</span></a>
        <nav id="at-nav" class="at-nav">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed at-skewstyle" data-toggle="collapse" data-target="#at-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="at-navigation" class="collapse navbar-collapse at-navigation">
                <ul>
                    <li class="{{Request::path() == '/' ? 'active': ''}}"><a href="{{url('/')}}"><span>Home</span></a></li>
                    <li {{--class="at-skewstyle menu-item-has-children current-menu-item active"--}} class="{{Request::path() == 'introductory-program' ? 'active' : ''}}">
                        <a href="#"><span>Program Information</span></a>
                        <ul class="sub-menu">
                            <li><a href="{{route('introductory')}}"><span>Introductory Program</span></a></li>
                        </ul>
                    </li>
                    <li class="{{Request::path() == 'apply-online' ? 'active':''}}"><a href="{{route('applyOnline')}}"><span>Apply Online</span></a></li>
                    <li  class="{{Request::is('program-alumni/*') ? 'active': ''}}">
                        <a href="javascript:void(0);"><span>Program Alumni</span></a>
                        <ul class="sub-menu">
                            @foreach(\App\Batch::get() as $batch)
                            <li ><a href="{{url($batch->url)}}"><span>{{$batch->batch_name}}</span></a></li>
                            @endforeach
                        </ul>
                    </li>
{{--                    {{dd(\App\ExcellenceCenterService::pluck('page_name','id'))}}--}}

                    <li class="{{ Request::path() == 'faculty' ? 'active' : '' }}"><a href="{{route('faculty')}}"><span>Faculty</span></a></li>

                    <li class="{{Request::is('excellence-center-services/*')  ? 'active' : ''}}" {{--class=" menu-item-has-children current-menu-item"--}}>
                        <a href="javascript:void(0);"><span>Excellence Center’s Services</span></a>
                        <ul class="sub-menu">
                            @foreach(\App\ExcellenceCenterService::get() as $link)
                                <li><a href="{{url($link->url)}}"><span>{{$link->page_name}}</span></a></li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="{{Request::path()== 'upcoming-courses' ? 'active': ''}}"><a href="{{route('upcomingCourses')}}"><span>Upcoming Courses</span></a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>

@extends('pakistan-strength.layouts.master')
@section('headings')
    <h1>Faculty</h1>--}}
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                Apply Online Start
        *************************************-->
        <div class="at-haslayout at-facultydetail">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="at-facultydetailholder">
                            <div class="at-leadershipbox">
                                <figure class="at-leaderimg">
                                    <img src="{{asset('uploads/avatars/'.$faculty_member->avatar)}}" alt="image description">
                                    <figcaption class="at-leadercontent">
                                        <h3>{{$faculty_member->name}}</h3>
                                        <span>Creative Director</span>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="at-facultydetailcontent">
                                <ul>
                                    <li>
                                        <span>Country:</span>
                                        <span>{{$faculty_member->country}}</span>
                                    </li>
                                    <li>
                                        <span>Sport:</span>
                                        <span>{{$faculty_member->sport}}</span>
                                    </li>
                                    <li>
                                        <span>Email:</span>
                                        <span>{{$faculty_member->email}}</span>
                                    </li>
                                    <li>
                                        <span>Sport:</span>
                                        <span>Swiming</span>
                                    </li>
                                    <li>
                                        <span>City:</span>
                                        <span>{{$faculty_member->city}}</span>
                                    </li>
                                </ul>
                                <div class="at-description">
                                    {{$faculty_member->description}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Apply Online End
        *************************************-->
    </main>
@endsection
@endsection

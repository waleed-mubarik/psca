@extends('pakistan-strength.layouts.master')
@section('title','Program Alumni Detail')
@section('headings')
    <h1>Program Alumni</h1>
@endsection
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                Program Alumni Start
        *************************************-->
        <div class="at-haslayout">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="at-programdetail">
                            <div class="at-aboutuscontentbox">
                                <div class="at-levelcertified">
                                    <div class="at-levelcertifiedheading">
                                        <h2><span>{{$student->name}}</span>PCSA Level Certified</h2>
                                    </div>
                                    <figure class="at-levelcertifiedimg"><img src="{{asset('uploads/students/avatars/'.$student->avatar)}}" alt="image description"></figure>
                                    <div class="at-programdetailcontent">
                                        <div class="at-universityheading">
                                            <figure><img src="{{asset('uploads/students/avatars/'.$student->avatar)}}" alt="image description"></figure>
                                            <h3>University of Delaware <span>Associate Director, ICECP </span></h3>
                                        </div>
                                        <ul>
                                            <li>
                                                <i class="fa fa-phone"></i>
                                                <span>{{$student->contact}}</span>
                                            </li>
                                            <li>
                                                <i class="fa fa-envelope-o"></i>
                                                <span>{{$student->email}}</span>
                                            </li>
                                        </ul>
                                        <div class="at-countrysportbox">
                                            <h4><span>Country:</span>{{$student->country}}</h4>
                                            <h4><span>Sport:</span>{{$student->sport}}</h4>
                                        </div>
                                    </div>
                                    <div class="at-description">
                                        <p> {{$student->description}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="at-achievements">
                                <h3>Achievements</h3>
                                <ul class="at-candidateproject">
                                    <li><span>Lorem ipsum dolor sit amet</span></li>
                                    <li><span>Tempor inde do s sectetur adipiscing elit</span></li>
                                    <li><span>Lorem ipsum dolor sit amet</span></li>
                                    <li><span>Tempor inde do s sectetur adipiscing elit</span></li>
                                    <li><span>Lorem ipsum dolor sit amet</span></li>
                                    <li><span>Tempor inde do s sectetur adipiscing elit</span></li>
                                </ul>
                            </div>
                            <div class="at-gallerybox">
                                <h3>Gallery</h3>
                                <ul class="at-gallery">
                                    <li>
                                        <figure><img src="images/gallery/img06.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img07.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img08.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img09.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img10.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img11.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img11.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img10.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img09.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img08.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img07.jpg" alt="image description"></figure>
                                    </li>
                                    <li>
                                        <figure><img src="images/gallery/img06.jpg" alt="image description"></figure>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Program Alumni End
        *************************************-->
    </main>
@endsection

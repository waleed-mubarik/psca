@extends('pakistan-strength.layouts.master')
@section('title','Program Alumni')
@section('headings')
    <h1>Program Alumni</h1>
@section('content')
    <main id="at-main" class="at-main at-haslayout">
        <!--************************************
                Program Alumni Start
        *************************************-->
        <div class="at-haslayout">
            <div class="at-programalumni">
                <div class="container">
                    <div class="row">
                        <div class="at-courseholder">
                            @foreach($batch->students as $student)
                            <div class="col-xs-6 col-sm-6 col-md-3">
                                <div class="at-coursebox">
                                    <figure class="at-courseimg">
                                        <a href="{{route('alumniDetail',$student->id)}}"><img src="{{asset('uploads/students/avatars/'.$student->avatar)}}" alt="images description"></a>
                                    </figure>
                                    <div class="at-coursecontent">
                                        <h2>{{$student->name}}<span>PCSA Level Certified</span></h2>
                                        <a href="{{route('alumniDetail',$student->id)}}" class="at-readmorebtn"><span>View Details</span><i class="icon-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--************************************
                Program Alumni End
        *************************************-->
    </main>
@endsection
@endsection

<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

       DB::table('roles')->delete();

        $roles = [
            ['id' => 1, 'role' => 'admin','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')],
            ['id' => 2, 'role' => 'faculty','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')],
            ['id' => 3, 'role'=>'student','created_at' => date('Y-m-d H:i:s'),'updated_at' => date('Y-m-d H:i:s')]
        ];

        foreach ($roles as $role){
            \App\Role::create($role);
        }

    }
}

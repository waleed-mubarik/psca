<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('users')->delete();

        \Illuminate\Support\Facades\DB::table('users')->insert([
            'id'=>1,
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'role_id'=> 1,
            'password'=> \Illuminate\Support\Facades\Hash::make('123123')
        ]);
    }
}
